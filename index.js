// console.log(`JS DOM - Manipulation`)

// [SECTION] Document Object Model (DOM)
	// Allows us to access or modify the properties of an HTML element in a webpage.
	// It is a standard on how to get, change, add, or delete HTML elements
	// We will be focusing only with DOM in terms of managing forms.


// For selecting HTML elements, we will be using the document.querySelector / getElementByID

		// Syntax:
			// document.querySelector("html element")

		// CSS Selectors
			// class selector (.);
			// id selector (#);
			// tag selector (html tag);
			// universal (*);
			// attribute ([attribute]);

	// querySelectorAll
	let universalSelector = document.querySelectorAll("*");

	// querySelector
	let singleUniversalSelector = document.querySelector("*");

	console.log(universalSelector);
	console.log(singleUniversalSelector);

	let classSelector = document.querySelectorAll(".full-name");
	console.log(classSelector);

	let singleClassSelector = document.querySelector(".full-name");
	console.log(singleClassSelector);

	// querySelectorAll = lahat ng ay ganung property, nilalabas
	// querySelector = kung sino yung maunang makita sa code (from top to bottom)

	let singleIdSelector = document.querySelector(".txt-first-name");
	console.log(singleIdSelector);

	let tagSelector = document.querySelectorAll("input");
	console.log(tagSelector);


	// Targetting span with an id element
	let spanSelector = document.querySelector("span[id]");
	console.log(spanSelector);



	// getElement
	// Syntax:
		// document.getElement(by what selector)
	let element = document.getElementById("fullName");
	console.log(element);

	element = document.getElementsByClassName("full-name");
	console.log(element);



// [SECTION] Event Listeners
	// Whenever a user interacts with a webpage, this action is considered as an event.
	// Working with events is large part of creatinng interactivity in a web page
	// Specific function that will be triggered if the event happen.

	// The function used is "addEventListener", it takes two arguments:
		// 1st argument = a string identifying the event
		// 2nd argument, function that the listener will trigger once the "specified event" occur

		let txtFirstName = document.querySelector("#txt-first-name");

		// Add event listener
		// arguments: (event string, function)
		txtFirstName.addEventListener("keyup", () => {
			console.log(txtFirstName.value);

			// innerHTML ay to insert sa gitna na pair tags sa HTML file
			// value ay yung iniinput
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		})

		let txtLastName = document.querySelector("#txt-last-name");
		txtLastName.addEventListener("keyup", () => {
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		})


// Activity part

		let textColor = document.getElementById("text-color");

		textColor.addEventListener("change", () => {
			spanSelector.style.color = textColor.value;
		})